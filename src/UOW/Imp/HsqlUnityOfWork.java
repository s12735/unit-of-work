package UOW.Imp;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import Base.Entity;
import Base.EntityState;
import UOW.IUnitOfWork;
import UOW.IUnitOfWorkRepository;

public class HsqlUnityOfWork implements IUnitOfWork {

	private Connection connection;
	Map<Entity, IUnitOfWorkRepository> entities = new LinkedHashMap<>();

	public HsqlUnityOfWork(Connection connection) {
		super();
		this.connection = connection;

		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void commit() {
		for (Entity entity : entities.keySet()) {
			switch (entity.getState()) {
			case Modified:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistRemove(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case UnChanged:
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void rollback() {
		entities.clear();
	}

	@Override
	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);
	}

	@Override
	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}

	@Override
	public void markAsChanged(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Modified);
		entities.put(entity, repository);
	}

	public void setEntity(Entity entity, IUnitOfWorkRepository repo) {
		this.entities.put(entity, repo);
	}

}
