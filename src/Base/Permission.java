package Base;

public enum Permission {
	Add(1), Delete(2), Edit(3);

	private int permisionID;
	
	private Permission(int id) {
		this.permisionID = id;
	}
	public int getPermissionID(){
		return this.permisionID;
	}
}
