package Base;

public enum Roles {
	Admin(1), User(0), Other(-1);
	
	private int roleID;
	
	private Roles(int id){
		this.roleID = id;
	}
	
	public int getRoleID(){
		return this.roleID;
	}
}
