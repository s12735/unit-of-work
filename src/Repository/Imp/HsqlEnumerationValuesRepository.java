package Repository.Imp;

import java.sql.Connection;
import java.sql.SQLException;

import Base.EnumerationValue;
import Repository.IEntityBuilder;
import Repository.IEnumetarionValueRepository;
import UOW.IUnitOfWork;

public class HsqlEnumerationValuesRepository 
extends Repository<EnumerationValue> implements IEnumetarionValueRepository {
	
	public HsqlEnumerationValuesRepository(Connection connection, IEntityBuilder<EnumerationValue> builder, IUnitOfWork uow) {
		super(connection,builder,uow);
	}

	@Override
	protected void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		update.setInt(1, entity.getIntKey());
		update.setString(2, entity.getStringKey());
		update.setString(3, entity.getValue());
		update.setString(4, entity.getEnumerationName());
	}

	@Override
	protected void setUpInsertQuery(EnumerationValue entity) throws SQLException {
		insert.setInt(1, entity.getIntKey());
		insert.setString(2, entity.getStringKey());
		insert.setString(3, entity.getValue());
		insert.setString(4, entity.getEnumerationName());
	}

	@Override
	protected String getTableName() {
		return "enumerations";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE enumerations SET (intKey, stringKey, value, enumerationName)=(?,?,?,?) WHERE id=?;";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO enumerations (intKey, stringKey, value, enumerationName) VALUES (?,?,?,?);";
	}
	
	@Override
	public EnumerationValue withName(String name) {
		return null;
	}

	@Override
	public EnumerationValue withKey(int key, String name) {
		return null;
	}

	@Override
	public EnumerationValue withStringKey(String key, String name) {
		return null;
	}

}
