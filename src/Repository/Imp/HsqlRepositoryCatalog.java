package Repository.Imp;

import java.sql.Connection;

import Repository.IEnumetarionValueRepository;
import Repository.IRepositoryCatalog;
import Repository.IUserRepository;
import UOW.IUnitOfWork;

public class HsqlRepositoryCatalog implements IRepositoryCatalog {
	private Connection connection;
	private IUnitOfWork uow;
	
	public HsqlRepositoryCatalog(Connection connection, IUnitOfWork uow) {
		this.connection = connection;
		this.uow = uow;
	}

	@Override
	public IUserRepository getUsers() {
		return new HsqlUserRepository(connection, new UserBuilder(), uow);
	}

	@Override
	public IEnumetarionValueRepository getEnumerations() {
		return null;
	}

	@Override
	public void commit(){
		uow.commit();
	}

}
