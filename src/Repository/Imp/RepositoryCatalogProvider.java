package Repository.Imp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Repository.IRepositoryCatalog;
import UOW.IUnitOfWork;
import UOW.Imp.HsqlUnityOfWork;

public class RepositoryCatalogProvider {
	private static String url = "jdbc:hsqldb:hsql://localhost";

	public static IRepositoryCatalog catalog() {

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new HsqlUnityOfWork(connection);
			IRepositoryCatalog catalog = new HsqlRepositoryCatalog(connection, uow);

			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
