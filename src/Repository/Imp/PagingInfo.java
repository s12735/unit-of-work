package Repository.Imp;

public class PagingInfo {
	 private int page, pageSize, total;

	    public PagingInfo(int page, int pageSize, int total) {
	        this.page = page;
	        this.pageSize = pageSize;
	        this.total = total;
	    }

	    public int getPage() {
	        return this.page;
	    }
	    public void setPage(int page) {
	       this.page = page;
	    }
	    public int getPageSize() {
	        return this.pageSize;
	    }
	    public void setPageSize(int pageSize) {
	        this.pageSize = pageSize;
	    }
	    public int getTotalCount() {
	        return this.total;
	    }
	    public void setTotalCount(int total) {
	        this.total = total;
	    }
}
