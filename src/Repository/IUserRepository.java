package Repository;

import java.util.List;

import Base.User;

public interface IUserRepository extends IRepository<User> {
	public List<User> withLogin(String login);

	public List<User> withLoginAndPassword(String login, String password);

	void setupPermissions(User user);
}
