package Repository;

import Base.EnumerationValue;

public interface IEnumetarionValueRepository extends IRepository<EnumerationValue> {
	
	EnumerationValue withName(String name);

	EnumerationValue withKey(int key, String name);

	EnumerationValue withStringKey(String key, String name);
}
