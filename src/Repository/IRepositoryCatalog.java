package Repository;

public interface IRepositoryCatalog {
	
	public IUserRepository getUsers();
	public IEnumetarionValueRepository getEnumerations();
	public void commit();
}